const express = require('express');
const router = express.Router();
const { v4: uuidv4 } = require('uuid');
const jwt = require('jsonwebtoken');
const { verify } = require('./verifyToken');

let users = [];

router.get('/', (req, res) => {
  try {
    res.status(200).send(users);
  } catch(err) {
    res.status(404).send("User not found");
  }
});


router.post('/register', (req, res) => {
  const user = req.body;
  const {username, password, isAdmin} = req.body;
  users.push({ ...user, id: uuidv4() });
  res.status(200).send(`User with ${user.username} added successfully`)
});

let refreshTokens = [];

router.post('/refresh', (req, res, next) => {
  //Take the refresh token from the user
  const refreshToken = req.body.token;

  //send error if there is no token its invalid
  if(!refreshToken) return res.status(401).send("You are not authenticated");
  if(!refreshTokens.includes(refreshToken)) {
    return res.status(403).send("Refresh token is not valid!");
  }
  jwt.verify(refreshToken, process.env.JWT_SEC, (err ,user) => {
    err & console.log(err);
    refreshTokens =  refreshTokens.filter((token) => token !== refreshToken);

    const newAccessToken = generateAccessToken(user);
    const newRefreshToken = generateRefreshToken(user);

    refreshTokens.push(newRefreshToken);

    res.status(200).send({
      acccessToken: newAccessToken,
      refreshToken: newRefreshToken
    })
  })

  //if everything is ok, create new access token, refresh token and end to user
})

const generateAccessToken = (user) => {
  return jwt.sign({ id: user.id, isAdmin: user.isAdmin },
    process.env.JWT_SEC, { expiresIn: "10m" });
}

const generateRefreshToken = (user) => {
  return jwt.sign({ id: user.id, isAdmin: user.isAdmin },
    process.env.JWT_SEC, { expiresIn: "10m" });
}

router.post('/login', (req, res) => {
  const { username, password, id} = req.body
  const user = users.find((u) => {
    return u.username === username && u.password === password;
  });
  if(user) {
    //Generate an access token
    const acccessToken = generateAccessToken(user);
    const refreshToken = generateRefreshToken(user);
    refreshTokens.push(refreshToken);
    
    res.json({
      username: user.username,
      isAdmin: user.isAdmin,
      acccessToken,
      refreshToken
    });
  } else {
    res.status(400).send("Username or password incorrect");
  }
});

router.patch('/update/:id', (req, res,next) => {
  const id = req.params.id;
  const { username, password, isAdmin } = req.body;
  const user = users.find((user) => user.id === id && user.isAdmin);

  if(username, password, isAdmin) {
    user.username = username;
    user.password = password;
    user.isAdmin = isAdmin;
  }
  res.send(`User with id ${id} successfully updated`);
});

router.delete('/delete/:userId', verify, (req, res) => {
  if (req.user.isAdmin === true) {
    res.status(200).json("User has been deleted.");
  } else {
    res.status(403).json("You are not allowed to delete this user!");
  }
});

router.post('/logout', verify, (req, res, next) => {
  const refreshToken = req.params.token;
  refreshTokens = refreshTokens.filter((token) => token !== refreshToken);
  res.status(200).send("Logout successfully")
})

module.exports = router;